"""
This is the people module and supports all the ReST actions for the
MESSAGES collection
"""

# System modules
from datetime import datetime

# 3rd party modules
from flask import make_response, abort
from db.config import db
from db.models import MessagesSql, MessagesSchema

def get_timestamp():
    return datetime.utcnow().strftime(("%Y-%m-%dT%H:%M:%S.%f%z"))


# Data to serve with our API
MESSAGES = {
    "1": {
        "id" : "1",
        "conversationId" : "1",
        "timestamp" : "2019-02-15T17:49:27.056574+00:00",
        "message_from" : '1',
        "message_to" : '100',
        "text" : "Oi, como posso lhe ajudar?",
    },

    "2": {
        "id" : "2",
        "conversationId" : "1",
        "timestamp" : "2019-02-15T17:49:28.056574+00:00",
        "message_from" : '100',
        "message_to" : '1',
        "text" : "Gostaria de saber meu saldo.",
    },

    "3": {
        "id" : "3",
        "conversationId" : "2",
        "timestamp" : "2019-02-15T17:50:28.056574+00:00",
        "message_from" : '1',
        "message_to" : '101',
        "text" : "Oi, como posso lhe ajudar?.",
    }
}


def read_all_from_conversation(conversationId):
    """
    This function responds to a request for /api/messages
    with the complete lists of messages of a conversation
    :param conversatioId:    id of the conversation
    :return:                 list of messages from the conversation
    """
    # Get the conversation requested
    conversation = MessagesSql.query \
                  .filter(MessagesSql.conversationId == conversationId)

    # Did we find a conversation?
    if conversation is not None:

        # Serialize the data for the response
        schema = MessagesSchema(many=True)
        data = schema.dump(conversation).data
        return data

    # Otherwise, nope, didn't find that conversation
    else:
        abort(404, 'Conversation not found for conversationId: ' +
                   '{conversationId}'.format(conversationId=conversationId))


def read_one(id):
    """
    This function responds to a request for /api/messages/{id}
    with one matching message
    :param id:      id of message to find
    :return:        message matching id
    """
    # Get the message requested
    messages = MessagesSql.query \
                  .filter(MessagesSql.id == id) \
                  .one_or_none()

    # Did we find a message?
    if messages is not None:

        # Serialize the data for the response
        schema = MessagesSchema()
        return schema.dump(messages).data

    # Otherwise, nope, didn't find that message
    else:
        abort(404, 'Message not found for Id: {id}'.format(id=id))


def create(message):
    """
    This function creates a new message in the messages structure
    based on the passed in message data
    :param message:     message to create in messages structure
    :return:            201 on success, 406 on message exists
    """
    id             = message.get("id", None)
    conversationId = message.get("conversationId", None)
    timestamp      = get_timestamp()
    message_from   = message.get("message_from", None)
    message_to     = message.get("message_to", None)
    text           = message.get("text", None)

    existing_message = MessagesSql.query \
                       .filter(MessagesSql.id == id) \
                       .one_or_none()

    # Can we insert this message?
    if existing_message is None:

        # Create a messages instance using the schema and the passed-in message
        schema = MessagesSchema()
        new_message = schema.load(message, session=db.session).data

        # Add the message to the database
        db.session.add(new_message)
        db.session.commit()

        # Serialize and return the newly created message in the response
        return schema.dump(new_message).data, 201

    # Otherwise, nope, message exists already
    else:
        abort(409, f'Message {id} exists already')
