"""
This is the people module and supports all the ReST actions for the
BOTS collection
"""

# System modules
from datetime import datetime

# 3rd party modules
from flask import make_response, abort
from db.config import db
from db.models import BotsSql, BotsSchema

# Data to serve with our API
BOTS = {
    "1": {
        "id": "1",
        "name": "Aureo"
    },
    "2": {
        "id": "2",
        "name": "R2D2"
    },
    "3": {
        "id": "3",
        "name": "Akinator"
    }
}


def read_all():
    """
    This function responds to a request for /api/bots
    with the complete lists of bots

    :return:        list of bots
    """
    # Create the list of bots from our data
    bots = BotsSql.query.all()

    # Serialize the data for the response
    schema = BotsSchema(many=True)
    data = schema.dump(bots).data
    return data


def read_one(id):
    """
    This function responds to a request for /api/bots/{id}
    with one matching bot
    :param id:      id of bot to find
    :return:        bot matching id
    """
    # Get the person requested
    bots = BotsSql.query \
        .filter(BotsSql.id == id) \
        .one_or_none()

    # Did we find a bot?
    if bots is not None:

        # Serialize the data for the response
        schema = BotsSchema()
        return schema.dump(bots).data

    # Otherwise, nope, didn't find that bot
    else:
        abort(404, 'Bot not found for Id: {id}'.format(id=id))


def create(bot):
    """
    This function creates a new bot in the bots structure
    based on the passed in bot data
    :param bot:     bot to create in bots structure
    :return:        201 on success, 406 on bot exists
    """
    id   = bot.get("id", None)
    name = bot.get("name", None)

    existing_bot = BotsSql.query \
        .filter(BotsSql.id == id) \
        .filter(BotsSql.name == name) \
        .one_or_none()

    # Can we insert this bot?
    if existing_bot is None:

        # Create a bots instance using the schema and the passed-in bot
        schema = BotsSchema()
        new_bot = schema.load(bot, session=db.session).data

        # Add the bot to the database
        db.session.add(new_bot)
        db.session.commit()

        # Serialize and return the newly created bot in the response
        return schema.dump(new_bot).data, 201

    # Otherwise, nope, bot exists already
    else:
        abort(409, f'Bot {id} {name} exists already')


def update(id, bot):
    """
    This function updates an existing bot in the bots structure
    Id is kept the same
    :param id:              id of a bot to update in the bots structure
    :param updated_bot:     bot infos to update
    :return:                updated bot structure
    """
    # Get the bot requested from the db into session
    update_bot = BotsSql.query.filter(
        BotsSql.id == id
    ).one_or_none()

    # Did we find a bot?
    if update_bot is not None:

        # turn the passed in bot into a db object
        schema = BotsSchema()
        update = schema.load(bot, session=db.session).data

        # Set the id to the bot we want to update
        update.id = update_bot.id

        # merge the new object into the old and commit it to the db
        db.session.merge(update)
        db.session.commit()

        # return updated bot in the response
        data = schema.dump(update_bot).data

        return data, 200

    # Otherwise, nope, didn't find that bot
    else:
        abort(
            404,
            "Bot not found for Id: {id}".format(id=id),
        )


def delete(id):
    """
    This function deletes a bot from the bots structure
    :param lname:   id of bot to delete
    :return:        200 on successful delete, 404 if not found
    """
    # Get the bot requested
    bot = BotsSql.query.filter(BotsSql.id == id).one_or_none()

    # Did we find a bot?
    if bot is not None:
        db.session.delete(bot)
        db.session.commit()
        return make_response(
            "Bot {id} deleted".format(id=id), 200
        )

    # Otherwise, nope, didn't find that bot
    else:
        abort(
            404,
            "Bot not found for Id: {id}".format(id=id),
        )
