import unittest
from client_api_bots import ClientAPIBots

# Data to serve with our API
BOTS = {
    "1": {
        "id": "1",
        "name": "Aureo"
    },
    "2": {
        "id": "2",
        "name": "R2D2"
    },
    "3": {
        "id": "3",
        "name": "Akinator"
    }
}

NEW_BOTS = {
    "4": {
        "id": "4",
        "name": "Test Bot #4"
    }
}


bots = ClientAPIBots('http://localhost:5000/api/bots')

def dict_to_list(dict):
    return [ v for v in dict.values() ]


class TestBots(unittest.TestCase):

    def test_valid_bots_read_all(self):
        self.assertEqual(bots.read_all(), dict_to_list(BOTS))


    def test_valid_bots_read_one(self):
        self.assertEqual(bots.read_one('1'), BOTS['1'])


    def test_valid_bots_create_and_delete(self):
        # adding a new bot and checking it's there
        bots.create('4', 'Test Bot #4')
        self.assertEqual(bots.read_one('4'), NEW_BOTS['4'])
        # deleting it and see if it was deleted
        bots.delete('4')
        self.assertEqual(bots.read_all(), dict_to_list(BOTS))


    def test_valid_bots_update(self):
        # changing name of id '3' to 'Nostradamus'
        old_bot = bots.read_one('3')
        old_name = old_bot.get('name')
        new_name = 'Nostradamus'
        new_bot = {'id' : '3', 'name' : new_name}
        bots.update('3', new_bot)
        received_name = bots.read_one('3').get('name')
        self.assertEqual(received_name, new_name)

        # changing name of back to old name
        bots.update('3', old_bot)
        received_name = bots.read_one('3').get('name')
        self.assertEqual(received_name, old_name)


if __name__ == '__main__':
    unittest.main()
