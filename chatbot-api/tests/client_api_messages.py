import requests

class ClientAPIMessages:

    def __init__(self, url):
        self.url = url


    def read_one(self, id):
        path_endpoint = self.url + '/' + id
        resp = requests.get(path_endpoint)
        return resp.json()


    def read_all_from_conversation(self, conversationId):
        path_endpoint = self.url + '?conversationId={}'.format(conversationId)
        resp = requests.get(path_endpoint)
        return resp.json()


    def create(self, new_id,
                    new_conversationId,
                    new_from,
                    new_to,
                    new_text):
        path_endpoint = self.url
        return requests.post(path_endpoint, json={
            'id'             : new_id,
            'conversationId' : new_conversationId,
            'message_from'   : new_from,
            'message_to'     : new_to,
            'text'           : new_text
        })
