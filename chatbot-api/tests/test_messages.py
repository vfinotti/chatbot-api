import unittest
from client_api_messages import ClientAPIMessages

# Data to serve with our API
MESSAGES = {
    "1": {
        "id" : "1",
        "conversationId" : "1",
        "timestamp" : "2019-02-15T17:49:27.056574+00:00",
        "message_from" : '1',
        "message_to" : '100',
        "text" : "Oi, como posso lhe ajudar?",
    },

    "2": {
        "id" : "2",
        "conversationId" : "1",
        "timestamp" : "2019-02-15T17:49:28.056574+00:00",
        "message_from" : '100',
        "message_to" : '1',
        "text" : "Gostaria de saber meu saldo.",
    },

    "3": {
        "id" : "3",
        "conversationId" : "2",
        "timestamp" : "2019-02-15T17:50:28.056574+00:00",
        "message_from" : '1',
        "message_to" : '101',
        "text" : "Oi, como posso lhe ajudar?.",
    }
}

MESSAGES_NEW = {
    "4": {
        "id" : "4",
        "conversationId" : "2",
        "timestamp" : "to_be_added_latter",
        "message_from" : '101',
        "message_to" : '1',
        "text" : "Gostaria de saber o valor da minha conta.",
    }
}


messages = ClientAPIMessages('http://localhost:5000/api/messages')

def dict_to_list(dict):
    return [ v for v in dict.values() ]


class TestMessages(unittest.TestCase):

    def test_valid_messages_read_all_from_conversation(self):
        conversation1 = [msg for msg in MESSAGES.values() if msg['conversationId'] == '1']
        self.assertEqual(messages.read_all_from_conversation('1'), conversation1)


    def test_valid_messages_read_one(self):
        self.assertEqual(messages.read_one('1'), MESSAGES['1'])


    def test_valid_messages_create(self):
        # create a variable for the new message
        new_message = MESSAGES_NEW['4']

        # create the message
        new_id             = new_message.get("id", None)
        new_conversationId = new_message.get("conversationId", None)
        new_from           = new_message.get("message_from", None)
        new_to             = new_message.get("message_to", None)
        new_text           = new_message.get("text", None)
        messages.create( new_id,
                         new_conversationId,
                         new_from,
                         new_to,
                         new_text)
        # Checking if it's there, removing the timestamp for that
        received_message = messages.read_one('4')
        received_message.pop('timestamp')
        new_message.pop('timestamp')
        self.assertEqual(received_message, new_message)


if __name__ == '__main__':
    unittest.main()
