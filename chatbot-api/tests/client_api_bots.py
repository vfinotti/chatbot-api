import requests

class ClientAPIBots:

    def __init__(self, url):
        self.url = url


    def read_all(self):
        path_endpoint = self.url
        resp = requests.get(path_endpoint)
        return resp.json()


    def read_one(self, id):
        path_endpoint = self.url + '/' + id
        resp = requests.get(path_endpoint)
        return resp.json()


    def create(self, new_id, new_name):
        path_endpoint = self.url
        return requests.post(path_endpoint, json={
            'id': new_id,
            'name': new_name,
        })


    def delete(self, id):
        path_endpoint = self.url + '/' + id
        return requests.delete(path_endpoint)


    def update(self, id, new_bot):
        path_endpoint = self.url + '/' + id
        new_name = new_bot.get('name')
        return requests.put(path_endpoint, json={
            'id': id,
            'name': new_name,
        })
