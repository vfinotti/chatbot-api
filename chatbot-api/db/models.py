from datetime import datetime
from .config import db, ma

class BotsSql(db.Model):
    __tablename__ = 'bots'
    id = db.Column(db.String(36), primary_key=True)
    name = db.Column(db.String(36), index=True)

class BotsSchema(ma.ModelSchema):
    class Meta:
        model = BotsSql
        sqla_session = db.session


class MessagesSql(db.Model):
    __tablename__ = 'messages'
    id = db.Column(db.String(36), primary_key=True)
    conversationId = db.Column(db.String(36))
    timestamp      = db.Column(
        db.DateTime, default=datetime.utcnow
        )
    message_from   = db.Column(db.String(36))
    message_to     = db.Column(db.String(36))
    text           = db.Column(db.String(300))

class MessagesSchema(ma.ModelSchema):
    class Meta:
        model = MessagesSql
        sqla_session = db.session
