import os
import datetime
from db.config import db
from db.models import BotsSql, MessagesSql

# Data to initialize database with
BOTS = [
    {
        "id": "1",
        "name": "Aureo"
    },
    {
        "id": "2",
        "name": "R2D2"
    },
    {
        "id": "3",
        "name": "Akinator"
    }
]

MESSAGES = [
    {
        "id" : "1",
        "conversationId" : "1",
        "timestamp" : "2019-02-15T17:49:27.056574Z",
        "from" : '1',
        "to" : '100',
        "text" : "Oi, como posso lhe ajudar?",
    },

    {
        "id" : "2",
        "conversationId" : "1",
        "timestamp" : "2019-02-15T17:49:28.056574Z",
        "from" : '100',
        "to" : '1',
        "text" : "Gostaria de saber meu saldo.",
    },

    {
        "id" : "3",
        "conversationId" : "2",
        "timestamp" : "2019-02-15T17:50:28.056574Z",
        "from" : '1',
        "to" : '101',
        "text" : "Oi, como posso lhe ajudar?.",
    }
]

def string_to_datetime(dt_string, dt_string_format):
    return datetime.datetime.strptime(dt_string, dt_string_format)

dbdir = basedir = os.path.abspath(os.path.join(os.path.dirname(__file__),"db"))
# Delete database file if it exists currently
if os.path.exists(os.path.join(dbdir, 'chatbot.db')):
    os.remove(os.path.join(dbdir, 'chatbot.db'))

# Create the database
db.create_all()

# Iterate over the BOTS structure and populate the database
for bot in BOTS:
    b = BotsSql(id=bot['id'], name=bot['name'])
    db.session.add(b)

# Iterate over the MESSAGES structure and populate the database
for message in MESSAGES:
    m = MessagesSql(id = message['id'],
                    conversationId = message['conversationId'],
                    timestamp = string_to_datetime(message['timestamp'], '%Y-%m-%dT%H:%M:%S.%fZ'),
                    message_from = message['from'],
                    message_to = message['to'],
                    text = message['text'])
    db.session.add(m)

db.session.commit()
