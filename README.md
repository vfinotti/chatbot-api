# API for Chatbot

## Overview
Repository containing scripts to create a chatbot API. It consist of a HTTP/REST backend and a database.

## Features
- **Python 3+**
-  **Flask microframework** to build web app's
-  **Connexion framework** to handle HTTP requests defined using OpenAPI/Swagger
-  SQLite server, with **SQLAlchemy** to handle requests and **Marshmallow** to (des)serialize data while interfacing with Connexion
-  **Python unittest** validation

## Instructions

### Installation
To install and configure, clone the repository, go to the sources directory and install the dependencies using *requirements.txt*:

```sh
$ git clone git@gitlab.com:vfinotti/chatbot-api.git
$ cd ./chatbot-api/chatbot-api
# creating a virtual environment and activate it
$ python3 -m venv env
$ source env/bin/activate
# install requirements
$ pip3 install -r requirements.txt

```

### Running the Server
In order to run the server it's first required that the database is initialized and populated:

```sh
$ python3 build_database.py

```

Done! Then the server can be initialized using *server.py*:

```sh
$ python3 server.py

```

This will run the server on http://localhost:5000/, with the API itself available on http://localhost:5000/api. A basic documentation is provided on http://localhost:5000/api/ui.


### Endpoints and Operations

The available operations accessible through the endpoints are as follows:

| Resource  | Action | HTTP Verb | URL Path                          | Description                                                              |
|-----------|--------|-----------|-----------------------------------|--------------------------------------------------------------------------|
| /bots     | Read   | Get       | /api/bots                         | Defines an unique URL to read all existing bots                          |
| /bots     | Read   | Get       | /api/bots/{id}                    | Defines an unique URL to read bot of an {id}                             |
| /bots     | Create | Post      | /api/bots                         | Defines an unique URL to create a new bot                                |
| /bots     | Update | Put       | /api/bots/{id}                    | Defines an unique URL to update a bot of an {id}                         |
| /bots     | Delete | Delete    | /api/bots/{id}                    | Defines an unique URL to delete a bot of an {id}                         |
| /messages | Read   | Get       | /api/messages/{id}                | Defines an unique URL to read message of an {id}                         |
| /messages | Read   | Get       | /api/messages?conversationId={id} | Defines an unique URL to get all messages from a conversation of an {id} |
| /messages | Create | Post      | /api/messages                     | Defines an unique URL to create a new message                            |

### Testing with a Browser

The *read* operations can be tested with a web browser:

| URL Path                          | Action                                  | Example                                             |
|-----------------------------------|-----------------------------------------|-----------------------------------------------------|
| /api/bots                         | Get all bots                            | http://localhost:5000/api/bots                      |
| /api/bots/{id}                    | Get bots of an {id}                     | http://localhost:5000/api/bots/1                    |
| /api/messages/{id}                | Get message of an {id}                  | http://localhost:5000/api/messages/1                |
| /api/messages?conversationId={id} | Get all messages of a conversation {id} | http://localhost:5000/api/messages?conversationId=1 |

### Testing with *unittest*
Tests using the [unittest](https://docs.python.org/3/library/unittest.html) package were prepared to test the API. They use two clients, one */bots* and another for */messages*. In order to run all tests, go to the sources directory and call unittest:

```sh
$ cd ./chatbot-api/chatbot-api
$ python3 -m unittest discover ./tests -v

```

## ToDo's

- Fault Tolerance
  - [ ] Create an ID validator, so invalid ID's would be detected
  - [ ] When creating a message, verify if the bot's 'id' exists in the bots database  
  - [ ] Create new users' id's automatically
  - [ ] Create new messages' id's automatically  
- Acessability
  - [ ] Implement "Content Negotiation" to support multiple resources representation (HTML, XML, JSON, CSV, YAML, etc.)  
- Testing
  - [ ] Create a container for testing and development purposes
  - [ ] Configure Gitlab CI
  - [ ] Implement Pylint static code analysis
  - [ ] Include return code from requests on the returned value of api_requests so the tests can detect the type of error
  - [ ] Get MESSAGES and BOTS dictionaries from external files (customize them and avoid inconsistencies)
- Others
  - [ ] Solve problems on build_database.py, since it needs to be run from the same directory it's located
  - [ ] Study and implement the schema for messages, so I can use "to" and "from" for variable names and set the timestamp in a custom format

## References

 1. REST Good practices: http://blog.caelum.com.br/rest-principios-e-boas-praticas/
 2. REST/HTTP implementation on Python: https://realpython.com/flask-connexion-rest-api/
 3. API integration in python: https://realpython.com/api-integration-in-python/
 4. Working with VirtualEnvs for production: https://code.tutsplus.com/pt/tutorials/understanding-virtual-environments-in-python--cms-28272
 5. Describing parameter using Swagger.io: https://swagger.io/docs/specification/describing-parameters/
 6. Infer persistence in API's with databases: https://realpython.com/flask-connexion-rest-api-part-2/
